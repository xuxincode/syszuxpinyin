#ifndef UTF8CHINESE_H
#define UTF8CHINESE_H

typedef struct utf8Ch
{
    const char * pin;
    const char * hz;
}utf8ChSt;

#define MAX_HANZI_NUMBER  19813

class utf8Chinese
{
public:
    utf8Chinese();
    const char * GetChPinYin(int index);
    const char * GetChCode(int index);

};

#endif // UTF8CHINESE_H
